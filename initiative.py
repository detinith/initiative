#!/usr/bin/python
# Python Script for Initiative Tracking

### Debug Mode
# Enable with 'True', disable with 'False'. When enabled,
# -this will print a confirmation message after each
# -'chunk' of code.
d_mode = True
if d_mode: print "Debug Mode enabled."

### Imports
import random, os, operator

if d_mode: print "Imports loaded."

### Blank Variables
select, iniList = "", []

if d_mode: print "Global variables loaded."

### Set Variables
# Order of opList is order of processing. "=" should be
# -first, to avoid overwriting any previously processed
# -health mods in the same input.
opList = ["=", "+", "-"]

if d_mode: print "opList loaded."

### Settings
# Backup file will save after each 'while' iteration and
# -delete on quit. If program is killed without proper
# -quit, file will remain until program restarted. User
# -will be queried whether to load or not.
f_ini_bak = ".ini.bak"	#backup file location
if d_mode: print "Backup setting loaded."

# Save file will save upon menu selection. This will be
# -a separate file from the backup. Loading it will insert
# -the contents into the current list
f_ini_sav = ".ini.sav"	#save file location
if d_mode: print "Save setting loaded."

### Classes
# Each entry to the initiative list will be an object
# -described by the "ini_entry" class. It's attributes
# -are modified through the user interaction with the
# -menu. Ultimately, this class should contain all
# -aspects of a D&D character relevant to its combat
# -ability.
class ini_entry(object):
	# Class description
	"Common class for an initiative member."
	# Function to instantiate class
	def __init__(self, name, initiative):
		self.name = name
		self.initiative = initiative
		self.health = 0
	# Modify health
	def health_mod(self, hp, mod):
		if ( mod == "=" ):self.health = hp
		elif ( mod == "+" ):self.health += hp
		elif ( mod == "-" ):self.health -= hp
if d_mode: print "ini_entry(object) declared."

### Functions
def ini_reset():	# reset initiative list
	if d_mode: print "ini_reset() beginning."
	iniList[:] = []
	if d_mode: print "ini_reset() ending."

def ini_clear():	# clears terminal window with a lot of prints
	if d_mode: print "ini_clear() beginning."
	for i in range(20):print
	if d_mode: print "ini_clear() ending."

def ini_quit(string):		# quit the program with message "string"
	if d_mode: print "ini_quit beginning."
	# Selects "q" in case called without user query
	select = "q"
	# Print quitting message
	print string
	# Delete backup file
	os.remove(f_ini_bak)
	if d_mode: print "ini_quit ending."

def ini_menu():		# prints menu
	# Prints a message above menu for certain previous choices
	if ( select == "r" ):print "Initiative reset."
	elif ( select == "s" ):print "Initiative saved."
	elif ( select == "l" ):print "Initiative loaded."
	# Prints actual user menu
	print "*******************"
	print "Initiative Tracker"
	print "(h)elp"
	print "(r)eset initiative"
	print "(s)ave"
	print "(l)oad"
	print "(q)uit"
	print "*******************"

def ini_help():	# prints help
	ini_clear()
	print "Initiative Tracker Help"
	print "\nBASIC ENTRY"
	print "Enter the character's name and his/her initiative"
	print " value in any order to add to the list. The \"#\""
	print " symbol can take the place of the initiative value"
	print " to generate a random value between 1 and 20.\n"
	print "MODIFYING INITIATIVE"
	print "Entering an existing character's name with a"
	print " different initiative value will modify that"
	print " character's initiative to the given value.\n"
	print "HEALTH TRACKING"
	print "Enter a character's name and its desired health"
	print " in any order to set its health. The health value"
	print " must be preceded by the \"=\" symbol. Setting"
	print " a character's health will occur before any of"
	print " the modifications."
	print "Note: Because it's processed from left to right,"
	print " only the final \"=\" value will apply before any"
	print " addition and subtraction."
	print "To add or subtract to a character's health, do"
	print " the same except preceding the value with the"
	print " \"+\" or \"-\" symbols respectively."
	print "Note: When a character's health is 0, it isn't displayed.\n"
	print "ALL-IN-ONE ENTRY"
	print "Any of these entries may be made all at the same time."
	print " In fact, any number of terms can be made in any"
	print " order for a single character. Any numbers preceded by"
	print " \"=\", \"+\", or \"-\" will modify the character's"
	print " health. The first number without one of those symbols"
	print " will be the initiative value. The first word that"
	print " doesn't fall under either of those conditions will be"
	print " its name. All other values will be ignored.\n\n"
	raw_input("'Enter' to return to menu:")

def ini_display():	# simply prints list
	print
	# If blank, don't bother reading any elements and state empty
	if ( iniList == [] ): print("Empty")
	# Otherwise, create and print a new list which is in order
	else:
		tempList = sorted(iniList, key=operator.attrgetter("initiative"), reverse=True)
		for objct in tempList:
			# Keep '0' values from being printed for brevity
			if objct.health:
				print str(objct.initiative) + "\t" + str(objct.name) + "\t" + str(objct.health)
			else:
				print str(objct.initiative) + "\t" + str(objct.name)
	print

def ini_check(check):	# integer check
	if d_mode: print "ini_check(" + str(check) + ") beginning."
	# True if integer, False if not
	try:
		tempInt = float(check)
		return True
	except:return False
	if d_mode: print "ini_check ending."

def ini_search(search, value):	# search designated aspect of objects for value
	if d_mode: print "ini_search(" + str(search) + ", " + str(value) + ") beginning."
	# Read aspect of each element for value
	# Return element and value of duplicates
	for element in iniList:
		if ( search == "initiative" ) and ( element.initiative == value ):
			if d_mode: print "Duplicate initiative found"
			return element, element.initiative
		elif ( search == "name") and ( element.name == value ):
			if d_mode: print "Duplicate name found"
			return element, element.name
	# No duplicates, return blank strings instead
	if d_mode: print "No duplicates found."
	return "", ""

def ini_mod(name, mod):	# apply list of mods to target
	if d_mode: print "ini_mod(" + str(name) + ", " + str(mod) + ") beginning."
	for item in iniList:
		if ( item.name == name ):
			for element in mod:
				item.health_mod(int(element[1:]), element[0])
	if d_mode: print "ini_mod ending."

def ini_add(addName, addInit):	# adding a name
	if d_mode: print "ini_add(" + str(addName) + ", " + str(addInit) + ") beginning."
	# Pre-search for duplicate values. No results will be a blank string (false)
	objctI, dInit = ini_search("initiative", addInit)
	objctN, dName = ini_search("name", addName)
	# Search for duplicate name
	if dName:
		objct = objctN
		# If init not given, only mods are to be looked at
		if ( addInit == "" ):
			if d_mode: print "Init blank, mods only"
			return
		# Search duplicate name for duplicate init
		elif dInit:
			# If found, ignore command with note
			if d_mode: print "ini_add - Duplicate name and init, rephrase entry"
			return
		else:
			# No duplicate init, overwrite init without note
			if d_mode: print "ini_add - Initiative to be rewritten"
			objct.initiative = addInit
			if d_mode: print "ini_add - Initiative rewritten"
			return
	# No duplicate name, search for duplicate init
	elif dInit:
		objct = objctI
		choice = ""
		# If found, query for who goes first
		while ( choice != "a" ) or ( choice != "b" ):
			if d_mode: print "ini_add - Need to determine which entry goes first"
			print "\"" + str(objct.name) + "\" has an equal intiative."
			print "Will the new entry be (a)head or (b)ehind?"
			choice = raw_input("a/b: ")
			if ( choice == "a" ):
				if d_mode: print "New entry goes ahead."
				tempInit = str(float(addInit) + 0.1).zfill(4)
				ini_add(addName, tempInit)
				return
			elif ( choice == "b" ):
				if d_mode: print "New entry goes behind."
				tempInit = str(float(addInit) - 0.1).zfill(4)
				ini_add(addName, tempInit)
				return
			else:print "Incorrect entry."
	# Add info
	addAdd = ini_entry(addName, addInit)
	iniList.append(addAdd)
	if d_mode: print "ini_add ending."

def ini_order(noOrder):	# list of mods
	if d_mode: print "ini_order(" + str(noOrder) + ") beginning."
	inOrder = []
	# Prioritize inputs
	for op in opList:
		for mod in noOrder:
			if ( mod[0] == op ):inOrder.append(mod)
	if d_mode: print "ini_order ending."
	return inOrder

def ini_remove(rmName):	# delete name
	if d_mode: print "ini_remove beginning"
	inst, name = ini_search("name", rmName)
	if d_mode: print "ini_remove starting list seek"
	for indx, objct in enumerate(iniList):
		if ( inst == objct ):
			if d_mode: print "Attempting index deletion"
			del(iniList[indx])
			if d_mode: print "Element deleted"
			return

def ini_parse(iniInput):	# parse input
	if d_mode: print "ini_parse(" + str(iniInput) + ") beginning."
	# Split input entries into individual list entries
	iniEntry, iniName, iniInit, iniMods = iniInput.split(" "), "", "", []
	if d_mode: print "ini_parse variables declared"
	# Check each list entry for any applicable information
	remove = False
	for element in iniEntry:
		if ( element[0] in opList ):
			iniMods.append(element)
		else:
			if ( element == "#" ) and ( iniInit == ""):
				iniInit = str(float(random.randint(1,20))).zfill(4)
			elif ( ini_check(element) ) and ( iniInit == ""):
				iniInit = str(float(element)).zfill(4)
			elif ( iniName == "" ) and ( element != "remove" ):
				iniName = element
			elif ( element == "remove" ):
				remove = True
		if remove and ( iniName != "" ):
			ini_remove(iniName)
			if d_mode: print iniName, "removed"
			return
	if d_mode: print "ini_parse variables parsed"
	# Attempt to add to list
	ini_add(iniName, iniInit)
	if d_mode: print "ini_parse ini_add ran"
	# Ask for an ordered mod list, if any mods, then process
	if ( iniMods != [] ):
		iniMods = ini_order(iniMods)
		ini_mod(iniName, iniMods)
	if d_mode: print "ini_parse ending."

def ini_restore(restore):	#replaces or appends current list with loaded one
	print "ini_restore(" + str(restore) + ") beginning."
	# Splits string into list of individual stat blocks
	tList = restore.split("|")
	for element in tList:
		ini_parse(element)
	if d_mode: print "ini_restore ending."

def ini_load(f_ini_loc, reset):	#loads the list from file
	print "ini_load beginning."
	# Reset initiative?
	if reset: ini_reset()
	# Load file and call restore
	f_load = open(f_ini_loc,"r")
	strLoad = f_load.read()
	f_load.close()
	ini_restore(strLoad)
	if d_mode: print "ini_load ending."

def ini_write():	#creates string out of creature stats
	if d_mode: print "ini_write() beginning."
	# Declare invented string
	strWrite = ""
	# Each object separated by '|'
	# Each stat separated by ' '
	# In future, each condition separated by ','
	for objct in iniList:
		strWrite += (str(objct.name) + " ")
		strWrite += (str(objct.initiative) + " ")
		strWrite += ("=" + str(objct.health))
#FUTURE		for con in objct.conditions:
#FUTURE			strWrite += (str(con) + ",")
		strWrite += "|"
	# Cuts off final '|' character to be clean
	strWrite = strWrite[:-1]
	# Debug-helpful print of str
	if d_mode: print strWrite
	# Returns string to be written
	if d_mode: print "ini_write ending."
	return strWrite

def ini_save(f_ini_loc):	#saves the list to file
	if d_mode: print "ini_save(" + str(f_ini_loc) + ") beginning."
	f_save = open(f_ini_loc,"w")
	f_save.write(ini_write())
	f_save.close()
	if d_mode: print "ini_save ending."

if d_mode: print "functions declared"

### Main
# Check for previous save, query to restore
if d_mode: print "Main beginning."
if ( os.path.exists(f_ini_bak) ):
	tChoice = raw_input("Save exists. Load? (y/n): ")
	if ( tChoice == "y" ):files.ini_load(f_ini_bak,1)
# While not quitting, keep going through the menu
while ( select != "q" ):
	# Backup save
	ini_save(f_ini_bak)
	# Clear the terminal
	ini_clear()
	# Print the menu
	ini_menu()
	# Keep the initiative up
	ini_display()
	# Ask for a selection
	select = raw_input('?: ')			# query for choice
	if ( select == "r" ):ini_reset()		# initiative reset
	elif ( select == "h" ):ini_help()		# help
	elif ( select == "s" ):ini_save(f_ini_sav)	# save
	elif ( select == "l" ):ini_load(f_ini_sav,0)	# load
	elif ( select == "q" ):ini_quit("User close")	# quit program
	else:
		try:ini_parse(select)			# parse
		except:ini_quit("Force closing")	# force close
if d_mode: print "Main ending."
